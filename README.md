# PGT Antibody GenBank entries from Walker 2011

Broad neutralization coverage of HIV by multiple highly potent antibodies.
Walker, L., Huber, M., Doores, K. et al.
Nature 477, 466–470 (2011).
<https://doi.org/10.1038/nature10373>

Donors and their antibodies:

 * 17: PGT121-123
 * 36: PGT125-131 
 * 39: PGT135-137
 * 84: PGT141-145

GenBank entries of antibody heavy and light chain sequences: JN201893-JN201927
