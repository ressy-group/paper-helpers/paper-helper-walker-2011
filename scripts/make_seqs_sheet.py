#!/usr/bin/env python

"""
Make CSV of antibody seq attributes from downloaded GenBank files.
"""

import re
import sys
from collections import defaultdict
from csv import DictReader, DictWriter
from Bio import SeqIO

def parse_seq_desc(txt):
    match = re.match(r"([A-Z0-9.]+) .*isolate (PGT[0-9]+) .*immunoglobulin (heavy|light) chain.*", txt)
    grps = match.groups()
    keys = ["Accession", "Antibody", "Chain"]
    fields = {k: v for k, v in zip(keys, grps)}
    return fields

FIELDS = ["Antibody", "Subject", "HeavyAccession", "LightAccession", "HeavySeq", "LightSeq"]

def make_seqs_sheet(fastas, path_s2):
    with open(path_s2) as f_in:
        mab_to_subject = {row["Name"]: row["Donor"] for row in DictReader(f_in)}
    mabs = defaultdict(dict)
    for fasta in fastas:
        for record in SeqIO.parse(fasta, "fasta"):
            fields = parse_seq_desc(record.description)
            col_acc = fields["Chain"].capitalize() + "Accession"
            col_seq = fields["Chain"].capitalize() + "Seq"
            mabs[fields["Antibody"]]["Antibody"] = fields["Antibody"]
            mabs[fields["Antibody"]]["Subject"] = mab_to_subject[fields["Antibody"]]
            mabs[fields["Antibody"]][col_acc] = fields["Accession"]
            mabs[fields["Antibody"]][col_seq] = str(record.seq)
    mabs = list(mabs.values())
    mabs.sort(key = lambda row: row["Antibody"])
    writer = DictWriter(sys.stdout, FIELDS, lineterminator="\n")
    writer.writeheader()
    writer.writerows(mabs)

if __name__ == "__main__":
    make_seqs_sheet(sys.argv[1:-1], sys.argv[-1])
