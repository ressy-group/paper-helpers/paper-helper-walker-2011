with open("from-paper/genbank_accessions.txt") as f_in:
    GB_ACCESSIONS = [line.strip() for line in f_in]

TARGET_GBF_FASTA = expand("from-genbank/{acc}.fasta", acc=GB_ACCESSIONS)

rule make_seqs_sheet:
    output: "output/seqs.csv"
    input:
        fastas=TARGET_GBF_FASTA,
        tableS2="from-paper/tableS2.csv"
    shell: "python scripts/make_seqs_sheet.py {input.fastas} {input.tableS2} > {output}"

rule all_gbf_fasta:
    input: TARGET_GBF_FASTA

rule download_gbf_fa:
    """Download one FASTA file per GenBank accession."""
    output: "from-genbank/{acc}.fasta"
    shell: "python scripts/download_ncbi.py nucleotide {wildcards.acc} {output}"
